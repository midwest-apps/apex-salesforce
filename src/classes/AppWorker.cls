global class AppWorker
{
    public static String MY_URL = 'http://example.com';

    public String Data {get; set;}

    // Security-ApexBadCrypto
    public without sharing class Foo {
        Blob hardCodedIV = Blob.valueOf('Hardcoded IV 123');
        Blob hardCodedKey = Blob.valueOf('0000000000000000');
        Blob data = Blob.valueOf('Data to be encrypted');
        Blob encrypted = Crypto.encrypt('AES128', hardCodedKey, hardCodedIV, data);
    }

    // Security-ApexCRUDViolation
    public class Foo {
        public Contact foo(String status, String ID) {
            Contact c = [SELECT Status__c FROM Contact WHERE Id=:ID];

            // Make sure we can update the database before even trying
            if (!Schema.sObjectType.Contact.fields.Name.isUpdateable()) {
                return null;
            }

            c.Status__c = status;
            update c;
            return c;
        }
    }

    // Security-ApexDangerousMethods
    public class Foo {
        public Foo() {
            Configuration.disableTriggerCRUDSecurity();
        }
    }

    // Security-ApexInsecureEndpoint
    // Security-ApexSharingViolations
    public without sharing class Foo {
        void foo() {
            HttpRequest req = new HttpRequest();
            req.setEndpoint('http://localhost:com');
        }
    }

    // Security-ApexOpenRedirect
    // Security-ApexSharingViolations
    public without sharing class Foo {
        String unsafeLocation = ApexPage.getCurrentPage().getParameters.get('url_param');
        PageReference page() {
        return new PageReference(unsafeLocation);
        }
    }

    // Security-ApexSOQLInjection
    public class Foo {
        public void test1(String t1) {
            Database.query('SELECT Id FROM Account' + t1);
        }
    }

    // Security-ApexSuggestUsingNamedCred
    public class NamedCredViolation
    {
        public void violation(String username, String password) {
            Blob headerValue = Blob.valueOf(username + ':' + password);
            String authorizationHeader = 'BASIC ' + EncodingUtil.base64Encode(headerValue);
            req.setHeader('Authorization', authorizationHeader);
        }
    }

    // Security-ApexXSSFromURLParam
    public without sharing class Foo {
        String unescapedstring = ApexPage.getCurrentPage().getParameters.get('url_param');
        String usedLater = unescapedstring;
    }
}
